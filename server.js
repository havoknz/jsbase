let path = require('path');
let express = require('express');
let webpackEnvironment = require('./server/webpackEnvironment');

let app = express();

if (process.env.NODE_ENV == 'dev') {
    webpackEnvironment(app, require('./webpack.dev.config.js'));
}

app.use('/static', express.static('static'));
app.use('/', express.static('public'));

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'static/index.html'));
});

let port = process.env.PORT || 3000;

app.listen(3000, () => {
    console.log(`Started listening on 127.0.0.1:${port}`);
});