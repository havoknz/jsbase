import React from 'react';
import ReactDOM from 'react-dom';
import Main from './components/main';

require('normalize-css');

ReactDOM.render(<Main />, document.getElementById('app'), () => {});
