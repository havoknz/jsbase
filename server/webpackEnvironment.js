let webpack = require('webpack');
let webpackDevMiddleware = require('webpack-dev-middleware');
let webpackHotMiddleware = require('webpack-hot-middleware');

module.exports = (app, config) => {
    let compiler = webpack(config);

    let devMiddleware = webpackDevMiddleware(compiler, {
        hot: true,
        noInfo: true,
        quiet: true,
        publicPath: config.output.publicPath,
        stats: {
            colors: true,
        }
    });

    app.use(devMiddleware);

    app.use(webpackHotMiddleware(compiler, {
        log: () => { }
    }));
}