let webpack = require('webpack');
let HtmlWebpackPlugin = require('html-webpack-plugin');
let FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
let CleanWebpackPlugin = require('clean-webpack-plugin');

let paths = {
    assets: __dirname + '/client'
}

module.exports = {
    entry: {
        app: [
            'webpack-hot-middleware/client?reload=true',
            paths.assets + '/app.tsx',
        ]
    },
    output: {
        filename: '[name].js',
        path: __dirname + '/static',
        publicPath: '/static'
    },
    devtool: 'cheap-module-eval-source-map',
    module: {
        rules: [
            { test: /\.s[ac]ss$/, use: ['style-loader', 'css-loader', 'sass-loader'] },
            {
                test: /\.tsx?$/,
                use: [
                    { loader: "babel-loader" },
                    {
                        loader: 'ts-loader',
                        options: { silent: true }
                    },
                    { loader: 'tslint-loader' }
                ],
                include: paths.assets                
            },
            { test: /\.png$/, loader: 'file-loader' }
        ],
    },
    resolve: {
        extensions: ['.webpack.js', '.json', '.scss', '.js', '.jsx', '.ts', '.tsx']
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: 'client/index.html',
            filename: 'index.html',
            inject: 'body'
        }),
        new CleanWebpackPlugin('static', {
            verbose: false
        }),
        new webpack.HotModuleReplacementPlugin(),
        new FriendlyErrorsWebpackPlugin({
            compilationSuccessInfo: {
                messages: ['Listening on http://localhost:3000']
            }
        })
    ]
};